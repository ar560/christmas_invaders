import pygame
import stage0
import stage1
import stage2
import stage3
import stageintro
import stageminusone
from pygame.locals import *
def main():
    pygame.init()
    screen = pygame.display.set_mode((500,600))
    pygame.display.set_caption("Christmas Invaders")
    stage =  0
    param = []
    #black background
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((0,0,0))
    #blit to screen
    screen.blit(background,(0,0))
    while True:
        screen.blit(background,(0,0))
        if stage == 0:#intro
            [param,stage] = stage0.blit(screen,param)
        elif stage == 1:#first shooter
            [param,stage] = stage1.blit(screen,param)
        elif stage == 2: # boss
            [param,stage] = stage2.blit(screen,param)
        elif stage == -1:#gameover
            [param,stage] = stageminusone.blit(screen,param)
        elif stage == 6:
            [param,stage] = stageintro.blit(screen,param)
        else: #outro
            [param,stage] = stage3.blit(screen,param)
        
        for event in pygame.event.get():
            if event.type == QUIT:
                return
            if stage == 0:
                param = stage0.eventhandle(event,param)
            elif stage == 1:
                param = stage1.eventhandle(event,param)
            elif stage == 2:
                param = stage2.eventhandle(event,param)
            elif stage == -1:
                param = stageminusone.eventhandle(event,param)
            elif stage == 3:
                param = stage3.eventhandle(event,param)
            elif stage == 6:
                param = stageintro.eventhandle(event,param)
            else:
                return
            
        pygame.display.flip()
    
main()

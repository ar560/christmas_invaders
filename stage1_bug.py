import pygame
import time
import os
import random
def blit(surface,param):
	#background
	backgroundpath = os.path.join('res','Background.png')
	background = pygame.image.load(backgroundpath)
	surface.blit(background,(0,0))
	#parameters
	if len(param) == 0:
		#print "param init"
		param = [[0,time.time(),0,-1,0],[0,5]]
		for j in range(4):
			print (j*120)
			param.append(35+j*120)
			param.append(400)
			param.append(8)
		for i in range(5):
			for j in range(10):
				param.append(175+32*j)
				param.append(10+48*i)
		
	#igloos
	igloopath = os.path.join('res','Igloo.png')
	igloo2path = os.path.join('res','Igloo_2.png')
	igloo3path = os.path.join('res','Igloo_3.png')
	igloo4path = os.path.join('res','Igloo_4.png')
	
	igloo_one= pygame.image.load(igloopath)
	igloo_two = pygame.image.load(igloo2path)
	igloo_three = pygame.image.load(igloo3path)
	igloo_four = pygame.image.load(igloo4path)
	
	
	#surface.blit(igloo,(200,200))
	for i in range(2,14,3):
		#print "igloo is ",param[i],',',param[i+1]
		if param[i+2] > 6:
			surface.blit(igloo_one,(param[i],param[i+1]))
		elif param[i+2] > 4:
			surface.blit(igloo_two,(param[i],param[i+1]))
		elif param[i+2] > 2:
			surface.blit(igloo_three,(param[i],param[i+1]))
		elif param[i+2] > 0:
			surface.blit(igloo_four,(param[i],param[i+1]))
	#enemiems
	elfpath = os.path.join('res','Elf.png')
	elf = pygame.image.load(elfpath)
	
	
	for i in range(14,114,2):
		surface.blit(elf,(param[i],param[i+1]))
		if random.random() < 0.0003:
			print param
			param[0].append(param[i])
			param[0].append(param[i+1])
		param[i] = param[i] + param[0][3]
	param[0][4] += 1
	counter = 0
	if param[0][4] > 174:
		print param[0][4]
		param[0][3] = -param[0][3]
		param[0][4] = 0
		counter += 1
	if (counter%2 != 0):
		print "yes"
		for i in range(14,114,2):
			param[i+1] += 10
	
	#enenmy fire
	canepath = os.path.join('res','Sugar_Cane.png')
	cane = pygame.image.load(canepath)
	if len(param[0]) > 5:
		for i in range(5,len(param[0])-1,2):
			try:
				surface.blit(cane,(param[0][i],param[0][i+1]))
				#print param
				#print param[0][i],param[0][i+1]
				param[0][i+1] += 3
				#candy cane reaching end of screen
				if param[0][i+1] > 600:
					param[0].pop(5)
					param[0].pop(5)
				#candy cane colliding with igloo
				for index in range(2,14,3):
					#print param[0][i]-param[index],param[0][i+1]-param[index+1] , param[0][i]-param[index] <32
					if param[0][i]-param[index] <64:
						if param[0][i]-param[index] > 0:
							if param[0][i+1]-param[index+1] < 33:
								if param[0][i+1]-param[index+1] >0:
									
									if param[index+2] > 0:
										print "cane removed"
										param[0].pop(i)
										param[0].pop(i)
										param[index+2] -= 1
				#candy cane colliding with player
				if param[0][i]-(250+param[0][0]) <64:
						if param[0][i]-(250+param[0][0]) > 0:
							if param[0][i+1]-500 < 33:
								if param[0][i+1]-500 >0:
									print "cane removed"
									param[0].pop(i)
									param[0].pop(i)
									param[1][1] -= 1
									##check if player has no hearts left
									if param[1][1] < 1:
										for i in range(14,114,2):
											if param[i-1] > 350:
												has_returned = True
												return [[],2]
								
			except IndexError:
				print "cane removed" ,i,i+1,len(param[0])			
			
	
	#carrots
	carrotpath = os.path.join('res','Carrot.png')
	carrot = pygame.image.load(carrotpath)
	#carrot detruction
	if len(param) > 2:
		for i in range(114,len(param)-1,2):
			try:
				surface.blit(carrot,(param[i],param[i+1]))
				param[i+1] -= 4
				#carrot reaching end of screen
				if param[i+1] < 0:
					param.pop(114)
					param.pop(114)
					print param
				#carrots coliding with igloos
				for index in range(2,14,3):
					if param[i]-param[index] <64:
						if param[i]-param[index] > 0:
							if param[i+1]-param[index+1] < 33:
								if param[i+1]-param[index+1] >0:
									print param[i]-param[index],param[i+1]-param[index+1] , param[i]-param[index] <32,
									if param[index+2] > 0:
										param.pop(i)
										param.pop(i)
										param[index+2] -= 1
				#carrots coliding with elfs(evil ones)
				for index in range(14,114,2):
					if param[i]-param[index] <32:
						if param[i]-param[index] > 0:
							if param[i+1]-param[index+1] < 32:
								if param[i+1]-param[index+1] >0:
									print param[i]-param[index],param[i+1]-param[index+1] , param[i]-param[index] <32,
									if param[index+2] > 0:
										param.pop(i)
										param.pop(i)
										param[index+1] = 650
				
								
					
				
			except IndexError:
				print "carrot removed" ,i,i+1,len(param)
			
			
	param[0][0] -= param[1][0]*3
	#guy 
	path = os.path.join('res','Christmas_Guy_64.png')
	guy = pygame.image.load(path)
	path_zero = os.path.join('res','sprite_0.png')
	guy_zero = pygame.image.load(path_zero)
	path_one = os.path.join('res','sprite_1.png')
	guy_one = pygame.image.load(path_one)
	path_two = os.path.join('res','sprite_2.png')
	guy_two = pygame.image.load(path_two)
	path_three = os.path.join('res','sprite_3.png')
	guy_three = pygame.image.load(path_three)
	path_four = os.path.join('res','sprite_4.png')
	guy_four = pygame.image.load(path_four)
	path_five = os.path.join('res','sprite_5.png')
	guy_five = pygame.image.load(path_five)
	path_six = os.path.join('res','sprite_6.png')
	guy_six = pygame.image.load(path_six)
	path_seven = os.path.join('res','sprite_7.png')
	guy_seven = pygame.image.load(path_seven)
	if param[1][0] ==1:
		guy_zero = pygame.transform.flip(guy_zero,1,0)
		guy_one = pygame.transform.flip(guy_one,1,0)
		guy_two = pygame.transform.flip(guy_two,1,0)
		guy_three = pygame.transform.flip(guy_three,1,0)
		guy_four = pygame.transform.flip(guy_four,1,0)
		guy_five = pygame.transform.flip(guy_five,1,0)
		guy_six = pygame.transform.flip(guy_six,1,0)
		guy_seven = pygame.transform.flip(guy_seven,1,0)
	if param[0][0] > 200:
		print "stop hammertime"
		param[0][0] = 200
		param[1][0] = 0
	elif param[0][0] < -245:
		print "its hammertime"
		param[0][0] = -245
		param[1][0] = 0
	else:
		param[0][0] -= param[1][0]*2
	if param[1][0] == 0:
		param[0][2] = -1
		surface.blit(guy,(250+param[0][0],500))
	else:
		param[0][2] += 1
		if param[0][2] < 4:
			surface.blit(guy_zero,(250+param[0][0],500))
		elif param[0][2] < 7:
			surface.blit(guy_one,(250+param[0][0],500))
		elif param[0][2] < 10:
			surface.blit(guy_two,(250+param[0][0],500))
		elif param[0][2] < 13:
			surface.blit(guy_three,(250+param[0][0],500))
		elif param[0][2] < 16:
			surface.blit(guy_four,(250+param[0][0],500))
		elif param[0][2] < 19:
			surface.blit(guy_five,(250+param[0][0],500))
		elif param[0][2] < 22:
			surface.blit(guy_six,(250+param[0][0],500))
		elif param[0][2] < 25:
			surface.blit(guy_seven,(250+param[0][0],500))
		else:
			param[0][2] = 0
			surface.blit(guy_zero,(250+param[0][0],500))
			
	#cooldown bar
	
	pygame.draw.rect(surface,(0,0,0),(385,560,100,18),0)
	pygame.draw.rect(surface,(0, 230, 0),(387,561,96,16),0)
	if time.time()-param[0][1] < 0.5:
		pygame.draw.rect(surface,(230,0,0),(387,561,96-((time.time()-param[0][1])*210),16),0)
	#hearts
	
	heartpath = os.path.join('res','present.png')
	heart  = pygame.image.load(heartpath)
	for i in range(1,param[1][1]+1):
		surface.blit(heart,(300+i*30,525))
	
	
	return [param,1]
def eventhandle(event,param):
	if event.type == pygame.KEYDOWN:
		if event.key == pygame.K_SPACE:
			print "Fire"
		if event.key == pygame.K_LEFT:
			if param[0][0] > -225:
				print "left down"
				param[1][0] = 1
				#param[0] -= 10
		if event.key == pygame.K_RIGHT:
			if param[0][0] < 225:
				param[1][0] = -1
	if event.type == pygame.KEYUP:
		if event.key == pygame.K_SPACE:
			if time.time()-param[0][1] > 0.5:
				print "Fire"
				param.append(250+param[0][0]+35)
				param.append(525)
				param[0][1] = time.time()
				print param
			
			
		if event.key == pygame.K_LEFT:
			print "left up"
			param[1][0] = 0
		if event.key == pygame.K_RIGHT:
			param[1][0] = 0
	
	
	has_returned = False
	for i in range(14,114,2):
		if param[i-1] > 370:
			has_returned = True
			return [[],2]
	if has_returned == False:
		return [param,1]
		
		
	

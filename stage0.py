import pygame
from pygame.locals import *
import os
def blit(surface,param):
    if len(param) == 0:
        param = [0]
    pygame.draw.rect(surface, (64,64,64), (25,500,440,75) , 0)
    path = os.path.join('res', 'Silouette_64.png')
    silhouette = pygame.image.load(path)
    surface.blit(silhouette,(30,505))
    space_invaders_font_path = os.path.join('res', 'space_invaders.ttf')
    myfont = pygame.font.Font(space_invaders_font_path, 13)
    text_res = ["Welcome to Christmas Space Invaders","This Christmas has gone a little wrong", "Santa thinks everyone is naughty", "Your job is to save Christmas"]
    if param[0] == len(text_res):
        return [ [0], 6 ]
    label = myfont.render(text_res[param[0]], 1, (255, 255, 255))
    surface.blit(label, (115, 525))
    return [param,0]

def eventhandle(event,param):

    if event.type == KEYDOWN:
        if event.key == K_SPACE:
            param[0] += 1
    return param
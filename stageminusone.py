import pygame
from pygame.locals import *
import os
def blit(surface,param):
    if len(param) == 0:
        param = [0]
    pygame.draw.rect(surface, (64,64,64), (25,500,450,75) , 0)
    silhouette_path = os.path.join('res', 'Silouette_santa.png')
    silhouette = pygame.image.load(silhouette_path)
    surface.blit(silhouette,(30,505))
    space_invaders_font_path = os.path.join('res', 'space_invaders.ttf' )
    myfont = pygame.font.Font(space_invaders_font_path, 13)
    myfonta = pygame.font.Font(space_invaders_font_path, 45)
    text_res = ["Well done!!", "You managed to defeat the yourself", "The evil santa can deliver the presents", "Now you are dead"]
    if param[0] == len(text_res):
        #print("should be quitting")
        pygame.quit()
        return [ [], 4 ]
    gameover = myfonta.render('GAME OVER',1,(255, 255, 255))
    label = myfont.render(text_res[param[0]], 1, (255, 255, 255))
    surface.blit(label, (130, 525))
    surface.blit(gameover, (120,280))
    return [param,-1]

def eventhandle(event,param):

    if event.type == KEYDOWN:
        if event.key == K_SPACE:
            param[0] += 1
    return param
